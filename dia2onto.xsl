<?xml version="1.0" encoding="UTF-8"?>

<!-- @title Transformation d'un dessin Dia UML en ontologie owl --> 
<!-- @comment La feuille prends en entrée le fichier XML généré par Dia dégzippé -->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
                xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" 
                xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" 
                xmlns:owl="http://www.w3.org/2002/07/owl#"
                xmlns:dia="http://www.lysator.liu.se/~alla/dia/"
                xmlns:ext="http://exslt.org/common"
                xmlns:xml="http://www.w3.org/XML/1998/namespace"
                version="1.0" 
                exclude-result-prefixes='xsl dia ext' >
    <xsl:output method="xml" indent='yes' encoding="utf-8" version="1.0" />

    <xsl:template match="/">
        <!-- Au départ tout commence par un tag RDF... -->
        <rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:owl="http://www.w3.org/2002/07/owl#">
            <!-- Ajout des namespaces au tag RDF -->
            <xsl:for-each select="//dia:object[@type='UML - Class' and dia:attribute[@name='name']/dia:string='#namespaces#']/dia:attribute[@name='attributes']/dia:composite[@type='umlattribute']">
                <!-- Pour chaque propriété de la pseudo-classe namespaces on ajoute un namespace au tag RDF : name = préfixe, value= uri -->
                <xsl:call-template name="add_namespace"/>
            </xsl:for-each> 
            <!-- Ensuite on crée une diste les namespaces pour plus tard -->
            <xsl:variable name="namespaces_list">
                <xsl:element name="list">
                    <item prefix='rdf' type='' uri='http://www.w3.org/1999/02/22-rdf-syntax-ns#'/>
                    <item prefix='rdfs' type='' uri='http://www.w3.org/2000/01/rdf-schema#'/>
                    <item prefix='owl' type='' uri='http://www.w3.org/2002/07/owl#'/>
                    <!-- Pour chaque propriété de la pseudo-classe namespaces on ajoute un namespace à l'élément racine de la liste : name = préfixe, value= uri -->
                    <xsl:for-each select="//dia:object[@type='UML - Class' and dia:attribute[@name='name']/dia:string='#namespaces#']/dia:attribute[@name='attributes']/dia:composite[@type='umlattribute']">
                        <xsl:element name="item">
                            <xsl:attribute name="prefix">
                                <xsl:call-template name="clean_string">
                                    <xsl:with-param name="string" select="dia:attribute[@name='name']"/>
                                </xsl:call-template>
                            </xsl:attribute>
                            <xsl:attribute name="type">
                                <xsl:call-template name="clean_string">
                                    <xsl:with-param name="string" select="dia:attribute[@name='type']"/>
                                </xsl:call-template>
                            </xsl:attribute>
                            <xsl:attribute name="uri">
                                <xsl:call-template name="clean_string">
                                    <xsl:with-param name="string" select="dia:attribute[@name='value']"/>
                                </xsl:call-template>
                            </xsl:attribute>
                        </xsl:element>
                    </xsl:for-each> 
                </xsl:element>
            </xsl:variable>
            <!-- Génération de l'entête owl:Ontology -->
            <xsl:call-template name="ontology_header"/>
            <!-- Génération des classes owl:Class -->
            <xsl:call-template name="classes">
                <xsl:with-param name="namespaces_list" select="ext:node-set($namespaces_list)"/>
            </xsl:call-template> 
            <!-- Génération des propriétés owl:ObjectProperties -->
            <xsl:call-template name="fetch_properties"> 
                <xsl:with-param name="namespaces_list" select="ext:node-set($namespaces_list)"/>
            </xsl:call-template>        
        </rdf:RDF>
    </xsl:template>
    
    <!-- Génération des classes de l'ontologie : on recherche les objets dia dont l'attribut type=UML - Class -->
    <xsl:template name="classes">
        <xsl:param name="namespaces_list"/>
        <!-- Pour chaque objet UML - Class on génère le RDF correspondant -->
        <xsl:for-each select="//dia:object[@type='UML - Class' and dia:attribute[@name='name']/dia:string!='#namespaces#' and dia:attribute[@name='name']/dia:string!='#properties#']"> 
            <owl:Class>
                <!-- URI de la Classe -->
                <xsl:call-template name="about_uri">
                    <xsl:with-param name="namespaces_list" select="$namespaces_list"/>
                    <xsl:with-param name="uri">
                        <xsl:call-template name="clean_string">
                            <xsl:with-param name="string" select="dia:attribute[@name='name']/dia:string"/>
                        </xsl:call-template>
                    </xsl:with-param>
                </xsl:call-template>
                <!-- Label en clair -->
                <rdfs:label>
                    <xsl:call-template name="clean_string">
                        <xsl:with-param name="string" select="dia:attribute[@name='stereotype']/dia:string"/>
                    </xsl:call-template>
                </rdfs:label> 
                <!-- Commentaire -->
                <rdfs:comment>
                    <xsl:call-template name="clean_string">
                        <xsl:with-param name="string" select="dia:attribute[@name='comment']/dia:string"/>
                    </xsl:call-template>
                </rdfs:comment> 
                <!-- Héritages -->
                <xsl:for-each select="../dia:object[@type='UML - Generalization' and dia:connections/dia:connection[@handle='1' and @to=current()/@id]]">
                    <rdfs:subClassOf>
                        <!-- Recherche de la classe parente -->
                        <xsl:variable name="parent_id" select="dia:connections/dia:connection[@handle='0']/@to"/>
                        <xsl:attribute name="rdf:resource">
                            <xsl:call-template name="normalize_uri">
                                <xsl:with-param name="uri">
                                    <xsl:call-template name="clean_string">
                                        <xsl:with-param name="string" select="../dia:object[@id=$parent_id]/dia:attribute[@name='name']/dia:string"/>
                                    </xsl:call-template>
                                </xsl:with-param>
                                <xsl:with-param name="namespaces_list" select="$namespaces_list"/>
                            </xsl:call-template>
                        </xsl:attribute>
                    </rdfs:subClassOf>
                </xsl:for-each>
            </owl:Class>
        </xsl:for-each>
    </xsl:template>
    
    <!-- Nettoyage d'une chaine dia : toutes les chaines sont de la forme #xxxx# il faut donc enlevr les # et éventuellement enlever les sauts de ligne et espaces avant -->
    <xsl:template name="clean_string">
        <xsl:param name="string"/>
        <xsl:value-of select="substring(normalize-space($string),2,string-length(normalize-space($string))-2)"/>
    </xsl:template>
    
    <!-- Génération d'un noeud namespace (c'est compliqué en XSL 1.0)...
        Il faut créer un tag bidon dans une variable qui porte comme nom le prefixe:dummy car on peut lui préciser le namespace associé au préfixe.
        Ensuite l'idée est de copier les sous-noeuds sur l'axe namespace (namesmpace::*) de l'élément bidon... et on renvoie la copie du noeud.
        On fait tou ça car on ne peux pas créer un attribut xmlns:prefixe car en XSLT 1.0 il ne connait pas le namespace de xmlns, et pour cause 
        c'est un attribut natif de XML.
    -->
    <xsl:template name="add_namespace">
        <!-- Extraction du préfixe nettoyé (attribut name)-->
        <xsl:variable name="prefix">
            <xsl:call-template name="clean_string">
                <xsl:with-param name="string" select="dia:attribute[@name='name']"/>
            </xsl:call-template>
        </xsl:variable>
        <!-- Extraction de l'URI nettoyée (attribut name)-->
        <xsl:variable name="namespaceURI">
            <xsl:call-template name="clean_string">
                <xsl:with-param name="string" select="dia:attribute[@name='value']"/>
            </xsl:call-template>
        </xsl:variable>
        <!-- Création du nom du tag prefixe:dummy -->
        <xsl:variable name="namespace" select="concat($prefix,':dummy')"/>
       
        <!-- Création d'une  variable qui contient un nouvel élément prefixe:dummy auquel on associe l'uri prefixe 
        Cet élément n'est jamais inséré au final -->
        <xsl:variable name="nsDummy">
            <xsl:element name="{$namespace}" namespace="{$namespaceURI}"/>
        </xsl:variable>
        <!-- On stocke dans la variable vNS tous les noeuds sur l'axe namespace (en fait il n'y en a qu'un : celui qui nous interesse : xmlns:prefixe='URI' -->
        <xsl:variable name="vNS" select="ext:node-set($nsDummy)/*/namespace::*"/>
        <!-- On copie les noeuds de la variable -->
        <xsl:copy-of select="$vNS"/>
        <!-- Si c'est la base de l'ontologie on rajoute xml:base=URI -->
        <xsl:if test="dia:attribute[@name='type']/dia:string='#base#'">
            <xsl:attribute name="xml:base">
                <xsl:value-of select="$namespaceURI"/>
            </xsl:attribute>
        </xsl:if>
        <!-- Version XSLT 2.0 c'est plus simple !!!
        <xsl:namespace name="{$prefix}">
            <xsl:call-template name="clean_string">
                <xsl:with-param name="string" select="dia:attribute[@name='value']"/>
            </xsl:call-template>
        </xsl:namespace> -->
    </xsl:template>
    
    <!-- Génération de l'attribut about ou ID selon l'URI passée en paramètre -->
    <xsl:template name="about_uri">
        <xsl:param name="uri"/>
        <xsl:param name="namespaces_list"/>
        
        <!-- On ne fait qu'appeller le template avec les bons tags short et long !! -->
        <xsl:call-template name="normalize_uri">
            <xsl:with-param name="uri" select="$uri"/>
            <xsl:with-param name="namespaces_list" select="$namespaces_list"/>
            <xsl:with-param name="base_attribute_long">rdf:about</xsl:with-param>
            <xsl:with-param name="base_attribute_short">rdf:ID</xsl:with-param>
        </xsl:call-template>
    </xsl:template>
    
    <!-- Normalisation d'une URI pour une expression correcte RDF d'une URI dans un attribut :
        Une URI doit être saisie dans dia :
        -soit directement par son suffixe si elle appartient à xml:base
        -soit par son préfixe défini dans le cadre namespaces
        
        La fonction renvoie : 
        -Si base_attribute_shot est défini et que l'URI est reliée à xml:base : base_attribute_shot="suffixe de l'URI"
        -Si base_attribute_shot est non défini et que l'URI est reliée à xml:base : #suffixe de l'URI"
        -Si base_attribute_long est défini et que l'URI est reliée à un préfixe autre que la base : base_attribute_long="URI absolue (résolue)"
        -Si base_attribute_long n'est pas défini et que l'URI est reliée à un préfixe autre que la base : URI absolue (résolue)
    -->
    <xsl:template name="normalize_uri">
        <xsl:param name="uri"/>
        <xsl:param name="namespaces_list"/>
        <xsl:param name="base_attribute_long"/>
        <xsl:param name="base_attribute_short"/>
        
        <!-- On éclate l'uri sur ':' -->
        <xsl:variable name="prefix">
            <!-- Calcul du préfixe -->
            <xsl:choose>
                <!-- Si il y a un ":"  alors on prend ce qui est avant -->
                <xsl:when test="substring-after($uri,':')">
                    <xsl:value-of select="substring-before($uri,':')"/>
                </xsl:when>
                <!-- Sinon on prend comme préfixe la base -->
                <xsl:otherwise>
                    <xsl:value-of select="$namespaces_list/list/item[@type='base']/@prefix"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="suffix">
            <!-- Calcul du suffixe -->
            <xsl:choose>
                <!-- Si il y a un ":" alors on prend ce qu'il y a après -->
                <xsl:when test="substring-after($uri,':')">
                    <xsl:value-of select="substring-after($uri,':')"/>
                </xsl:when>
                <!-- Sinonn on garde l'uri entière -->
                <xsl:otherwise>
                    <xsl:value-of select="$uri"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        
        <!-- Calcul de l'URI finale -->
        <xsl:choose>
            <!-- Si le préfixe est la base -->
            <xsl:when test="$namespaces_list/list/item[@prefix=$prefix]/@type='base'">
                <xsl:choose>
                    <!-- Si l'attribut short est défini alors on renvoie l'attribut avec comme valeur le suffixe -->
                    <xsl:when test="$base_attribute_short">
                        <xsl:attribute name="{$base_attribute_short}">
                            <xsl:value-of select="$suffix"/>
                        </xsl:attribute>
                    </xsl:when>
                    <!-- Si l'attribut short n'est pas défini, on renvoie l'URI courte : #suffixe -->
                    <xsl:otherwise>
                        <xsl:value-of select="concat('#',$suffix)"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <!-- Si le préfixe n'est pas la base -->
            <xsl:otherwise>
                <!-- On récupère l'URI liée au préfixe -->
                <xsl:variable name="prefixURI">
                    <xsl:value-of select="$namespaces_list/list/item[@prefix=$prefix]/@uri"/>
                </xsl:variable>
                <xsl:choose>
                    <!-- Si l'attribut long est défini alors on renvoie l'attribut avec comme valeur l'URI absolue (URI préfixe+suffixe) -->
                    <xsl:when test="$base_attribute_long">
                        <xsl:attribute name="{$base_attribute_long}">
                            <xsl:value-of select="concat($prefixURI,$suffix)"/>
                        </xsl:attribute>
                    </xsl:when>
                    <!-- Si l'attribut long n'est pas défini alors on renvoie l'URI absolue (URI préfixe+suffixe) -->
                    <xsl:otherwise>
                        <xsl:value-of select="concat($prefixURI,$suffix)"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <!-- Calcul de l'URI systématiquement préfixée y compris avec le préfixe de la base (permet d'identifier des URI similaires -->
    <xsl:template name="absolute_uri">
        <xsl:param name="uri"/>
        <xsl:param name="namespaces_list"/>
        <!-- Ajout systématique du préfixe -->
        <xsl:choose>
            <!-- Si l'URI est déjà préfixée, on la renvoie telle qielle -->
            <xsl:when test="substring-before($uri,':')">
                <xsl:value-of select="$uri"/>
            </xsl:when>
            <!-- Sinon on ajoute le préfixe de la base -->
            <xsl:otherwise>
                <xsl:value-of select="concat($namespaces_list/list/item[@type='base']/@prefix,':',$uri)"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <!-- Calcul du header de l'ontologie (ow:Ontology) 
        Le header est décrit dans une note, une ligne par champ, chaque ligne commence par le nom du champ suivi de  ":" et de la valeur.
        Ex :    Ontology : Ontology de test . Les lignes vides et les espaces sont ignorés. La note doit commencer par "Ontology"
        Les champs possibles sont :
        -Ontology : l'URI de l'ontologie
        -Version : chaine de caractère précisant la version
        -Name : le nom (label)
        -Comment : la description.
        Le champ comment peut contenir plusieurs lignes mais dans ce cas il doit impérativement être en dernier. -->
        
    <xsl:template name="ontology_header">
        <!-- On parcours les notes à la recherche de celle qui commence par Ontology -->
        <xsl:for-each select="//dia:object[@type='UML - Note']">
            <xsl:variable name="header_content" select="dia:attribute[@name='text']/dia:composite[@type='text']/dia:attribute[@name='string']/dia:string"/>
            <xsl:if test="substring($header_content,1,9)='#Ontology'">
                <!-- On a trouvé ! on crée la zone owl:Ontology -->
                <xsl:element name="owl:Ontology">
                    <!-- On appele le template récursif qui analyse les lignes et génère le contenu -->
                    <xsl:call-template name="explode_header">
                        <xsl:with-param name="header" select="substring($header_content,1,string-length($header_content)-1)"/>
                    </xsl:call-template>
                </xsl:element>
            </xsl:if>
        </xsl:for-each>
    </xsl:template>
    
    <!-- Analyse chaque ligne de la note Ontology et crée les déclarations correspondantes -->
    <xsl:template name="explode_header">
        <xsl:param name="last_info"/> <!-- Dernier champ identifié -->
        <xsl:param name="header"/> <!-- Le reste de la note à analyser -->
        <!-- On extrait la ligne en prenant ce qui est avant le caractère &#10; 
        ou tout si il n'y a a pa de saut de ligne et on stocke dans la variable $line -->
        <xsl:variable name="line">
            <xsl:choose>
                <xsl:when test="normalize-space($header) and normalize-space(substring-after($header,'&#10;'))=''">
                    <xsl:value-of select='normalize-space($header)'/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="normalize-space(substring-before($header,'&#10;'))"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:choose>
            <xsl:when test="$line">
                <!-- y a-t-il un nom de champ ? -->
                <xsl:variable name="field">
                    <xsl:if test="normalize-space(substring-before($line,':'))">
                        <xsl:value-of select="normalize-space(substring-before($line,':'))"/>
                    </xsl:if>
                </xsl:variable>
                <xsl:choose>
                    <!-- Si il y a un nom de champ -->
                    <xsl:when test="$field!=''">
                        <xsl:choose>
                            <!-- Name -> rdfs:label -->
                            <xsl:when test="$field='Name'">
                                <rdfs:label>
                                    <xsl:value-of select="normalize-space(substring-after($line,':'))"/>
                                </rdfs:label>
                            </xsl:when>
                            <!-- Ontology -> attribut about de l'élément owl:Ontology -->
                            <xsl:when test="$field='#Ontology'">
                                <xsl:attribute name="rdf:about">
                                    <xsl:value-of select="normalize-space(substring-after($line,':'))"/>
                                </xsl:attribute>
                            </xsl:when>
                            <!-- Comment -> rdfs:comment -->
                            <xsl:when test="$field='Comment'">
                                <rdfs:comment>
                                    <xsl:value-of select="normalize-space(substring-after($line,':'))"/>
                                    <!-- Si la ligne suivante ne commence pas par un champ, c'est la suite du Comment a aggréger. On rappel le template avec la suite
                                    en remplissant le paramètre last_info -->
                                    <xsl:if test="substring-after($header,'&#10;') and normalize-space(substring-before(normalize-space(substring-before(substring-after($header,'&#10;'),'&#10;')),':'))=''">
                                        <xsl:call-template name="explode_header">
                                            <xsl:with-param name="last_info" select="$field"/>
                                            <xsl:with-param name="header" select="substring-after($header,'&#10;')"/>
                                        </xsl:call-template>
                                    </xsl:if>
                                </rdfs:comment>
                            </xsl:when>
                            <!-- Version -> owl:versionInfo --> 
                            <xsl:when test="$field='Version'">
                                <owl:versionInfo>
                                    <xsl:value-of select="normalize-space(substring-after($line,':'))"/>
                                </owl:versionInfo>
                            </xsl:when>
                        </xsl:choose>
                        <!-- Si il y a encore du texte après la ligne, on rappelle récursivement le template pour continuer l'analyse -->
                        <xsl:if test="substring-after($header,'&#10;')">
                            <xsl:call-template name="explode_header">
                                <xsl:with-param name="last_info"/>
                                <xsl:with-param name="header" select="substring-after($header,'&#10;')"/>
                            </xsl:call-template>
                        </xsl:if>
                    </xsl:when>
                    <!-- Si la ligne ne commence pas par un champ -->
                    <xsl:otherwise>
                        <!-- Si il n'y a pas de champ et que last_info est dfini, on aggrège à la ligne précédente de comment avec un saut de ligne-->
                        <xsl:if test="$last_info and $line!=''">
                            <xsl:value-of select="concat('&#10;',$line)"/>
                        </xsl:if>
                        <!-- Si il y a encore du texte à traiter, on rappelle récursivement le template.
                            Si last_info n'est pas défini, on ignore la ligne en fait !! -->
                        <xsl:if test="substring-after($header,'&#10;')">
                            <xsl:call-template name="explode_header">
                                <xsl:with-param name="last_info" select="$last_info"/>
                                <xsl:with-param name="header" select="substring-after($header,'&#10;')"/>
                            </xsl:call-template>
                        </xsl:if>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <!-- Si c'est une ligne vide et qu'il y a encore du texte après, on rappelle récursivement le template -->
            <xsl:otherwise>
                <xsl:if test="substring-after($header,'&#10;')">
                    <xsl:call-template name="explode_header">
                        <xsl:with-param name="last_info" select="$last_info"/>
                        <xsl:with-param name="header" select="substring-after($header,'&#10;')"/>
                    </xsl:call-template>
                </xsl:if>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <!-- Récupération de l'URI préfixée du range d'une propriété 
        Pour préciser le range, il suffit de mettre dans la case "type" de dia l'URI du range associé 
        Pour préciser un Literal, il faut mettre "literal" -->
    <xsl:template name="get_range">
        <xsl:param name="type"/>
        <xsl:param name="namespaces_list"/>
        <xsl:choose>
            <!-- Si le type est literal, on renvoie rdfs:Literal -->
            <xsl:when test="$type='#literal#'">
                <xsl:text>rdfs:Literal</xsl:text>
            </xsl:when>
            <!-- Sinon on calcule l'URI préfixée -->
            <xsl:otherwise>
                <xsl:call-template name="absolute_uri">
                    <xsl:with-param name="namespaces_list" select="$namespaces_list"/>
                    <xsl:with-param name="uri">
                        <xsl:call-template name="clean_string">
                            <xsl:with-param name="string" select="$type"/>
                        </xsl:call-template>
                    </xsl:with-param>
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <!-- Génération des propriétés -->
    <xsl:template name="fetch_properties">
        <xsl:param name="namespaces_list"/>
        <!-- Nombre de classes déclarées (hors namespaces et properties) -->
        <xsl:variable name="classescount">
            <xsl:value-of select="count(//dia:object[@type='UML - Class'])-2"/>
        </xsl:variable>
        <!-- Création d'une liste des propriétés (properties) en listant les propriétés des classes puis les associations entre classes -->
        <xsl:variable name="properties">
            <properties>
                <!-- propriétés citées directement dans les classes -->
                <xsl:for-each select="//dia:object[@type='UML - Class' and dia:attribute[@name='stereotype']/dia:string!='#namespaces#']/dia:attribute[@name='attributes']/dia:composite[@type='umlattribute']">
                    <property>
                        <!-- attribut uri -->
                        <xsl:attribute name="uri">
                            <xsl:call-template name="absolute_uri">
                                <xsl:with-param name="namespaces_list" select="$namespaces_list"/>
                                <xsl:with-param name="uri">
                                    <xsl:call-template name="clean_string">
                                        <xsl:with-param name="string" select="dia:attribute[@name='name']/dia:string"/>
                                    </xsl:call-template>
                                </xsl:with-param>
                            </xsl:call-template>
                        </xsl:attribute>
                        <!-- attribut class (la classe de rattachement) -->
                        <xsl:attribute name="class">
                            <xsl:call-template name="clean_string">
                                <xsl:with-param name="string" select="ancestor::dia:object/dia:attribute[@name='name']/dia:string"/>
                            </xsl:call-template>
                        </xsl:attribute>
                         <!-- attribut stereotype (le stereotype de la classe de rattachement) -->
                        <xsl:attribute name="stereotype">
                            <xsl:call-template name="clean_string">
                                <xsl:with-param name="string" select="ancestor::dia:object/dia:attribute[@name='stereotype']/dia:string"/>
                            </xsl:call-template>
                        </xsl:attribute>
                        <!-- attribut type (le type de valeurs possible pour cette propriété -->
                        <xsl:attribute name="type">
                            <xsl:choose>
                                <xsl:when test="dia:attribute[@name='type']/dia:string!='##'" >
                                    <xsl:call-template name="get_range">
                                        <xsl:with-param name="type" select="dia:attribute[@name='type']/dia:string"/>
                                        <xsl:with-param name="namespaces_list" select="$namespaces_list"/>
                                    </xsl:call-template>
                                </xsl:when> 
                                <!-- Si le type est vide on conserve la valeur '##' -->
                                <xsl:otherwise>
                                    <xsl:text>##</xsl:text>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:attribute>
                        <!-- si l'ancêtre de cette propriété est la classe properties on regarde 
                        si il y a un label et un commentaire pour cette propriété -->
                        <xsl:if test="ancestor::dia:object/dia:attribute[@name='stereotype']/dia:string='#properties#'">
                            <xsl:attribute name="label">
                                <xsl:call-template name="clean_string">
                                    <xsl:with-param name="string" select="dia:attribute[@name='value']/dia:string"/>
                                </xsl:call-template>
                            </xsl:attribute>
                            <xsl:attribute name="comment">
                                <xsl:call-template name="clean_string">
                                    <xsl:with-param name="string" select="dia:attribute[@name='comment']/dia:string"/>
                                </xsl:call-template>
                            </xsl:attribute>
                        </xsl:if>
                    </property>
                </xsl:for-each>
                <!-- propriétés crées par des relations -->
                <xsl:for-each select="//dia:object[@type='UML - Association']">
                    <property>
                        <!-- L'URI est le nom de la relation d'association -->
                        <xsl:attribute name="uri">
                            <xsl:call-template name="absolute_uri">
                                <xsl:with-param name="namespaces_list" select="$namespaces_list"/>
                                <xsl:with-param name="uri">
                                    <xsl:call-template name="clean_string">
                                        <xsl:with-param name="string" select="dia:attribute[@name='name']/dia:string"/>
                                    </xsl:call-template>
                                </xsl:with-param>
                            </xsl:call-template>
                        </xsl:attribute>
                        <!-- La classe est la classe de rattachement du départ de la relation -->
                        <xsl:attribute name="class">
                            <xsl:call-template name="clean_string">
                                <xsl:with-param name="string" select="../dia:object[@type='UML - Class' and @id=current()/dia:connections/dia:connection[@handle='0']/@to]/dia:attribute[@name='name']/dia:string"/>
                            </xsl:call-template>
                        </xsl:attribute>
                        <!-- Le stereotype est vide -->
                        <xsl:attribute name="stereotype"/>
                        <!-- Le type est la classe de rattachement à l'arrivée de l'association -->
                        <xsl:attribute name="type">
                             <xsl:call-template name="absolute_uri">
                                <xsl:with-param name="namespaces_list" select="$namespaces_list"/>
                                <xsl:with-param name="uri">
                                    <xsl:call-template name="clean_string">
                                        <xsl:with-param name="string">
                                            <xsl:value-of select="../dia:object[@type='UML - Class' and @id=current()/dia:connections/dia:connection[@handle='1']/@to]/dia:attribute[@name='name']/dia:string"/>
                                        </xsl:with-param>
                                    </xsl:call-template>
                                </xsl:with-param>
                            </xsl:call-template>
                        </xsl:attribute>
                    </property>
                </xsl:for-each>
            </properties>
        </xsl:variable>
        <!-- La liste des générée, maintenant on dédoublonne : pour chaque élément de la liste dont l'URI n'a pas déjà été citée -->
        <xsl:for-each select="ext:node-set($properties)/properties/property[not(@uri=preceding-sibling::property/@uri)]">
            <!-- On créer la propriété owl:ObjectProperty -->
            <xsl:element name="owl:ObjectProperty">
                <xsl:call-template name="about_uri">
                    <xsl:with-param name="uri" select="@uri"/>
                    <xsl:with-param name="namespaces_list" select="$namespaces_list"/>
                </xsl:call-template>
                <!-- On repasse en revue touts les autres éléments de la liste qui ont la même uri pour générer 
                les ranges et au passage les libellés et ommentaires -->
                <xsl:for-each select="ext:node-set($properties)/properties/property[@uri=current()/@uri]">
                    <!-- Si il y a un type et que c'est le premier, on rajoute un rdfs:range avec le type -->
                    <xsl:if test="@type!='##' and count(preceding-sibling::property[@uri=current()/@uri and @type=current()/@type])=0">
                        <rdfs:range>
                            <xsl:attribute name="rdf:resource">
                                <xsl:call-template name="normalize_uri">
                                    <xsl:with-param name="namespaces_list" select="$namespaces_list"/>
                                    <xsl:with-param name="uri" select="@type"/>
                                </xsl:call-template>
                            </xsl:attribute>
                        </rdfs:range>
                    </xsl:if>
                    <!-- Au passage, si c'est une propriété de la classe properties, on ajoute éventuellement le label et le comment -->
                    <xsl:if test="@stereotype='properties'">
                        <xsl:if test="@label!=''">
                            <rdfs:label>
                                <xsl:value-of select="@label"/>
                            </rdfs:label>
                        </xsl:if>
                        <xsl:if test="@comment!=''">
                            <rdfs:comment>
                                <xsl:value-of select="@comment"/>
                            </rdfs:comment>
                        </xsl:if>
                    </xsl:if>
                </xsl:for-each>
                <!-- Calcul des domaines -->
                <!-- Si la propriété est citée dans moins de classes que le total des classes alors on pécise les domaines 
                    sinon est elle autorisé partout et donc pas besoin de domaine -->
                <xsl:if test="count(ext:node-set($properties)/properties/property[@uri=current()/@uri and @stereotype!='properties'])&lt;$classescount and count(ext:node-set($properties)/properties/property[@uri=current()/@uri and @stereotype!='properties'])>0">
                    <xsl:for-each select="ext:node-set($properties)/properties/property[@uri=current()/@uri and @stereotype!='properties']">
                        <rdfs:domain>
                            <xsl:attribute name="rdf:resource">
                                <xsl:call-template name="normalize_uri">
                                    <xsl:with-param name="namespaces_list" select="$namespaces_list"/>
                                    <xsl:with-param name="uri" select="@class"/>
                                </xsl:call-template>
                            </xsl:attribute>
                        </rdfs:domain>
                    </xsl:for-each> 
                </xsl:if>
            </xsl:element>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>
